'''
Created on 22 nov. 2020

@author: RSSpe
'''


class Vertice:

    def __init__(self, nombre):
        self.nombre = nombre
        self.__numVertice = -1

    @property
    def numVertice(self):
        return self.__numVertice
    
    @numVertice.setter
    def numVertice(self, numVertice):
        self.__numVertice = numVertice

    def equals(self, n):
        return self.nombre == n.nombre

    def asigVert(self, n):
        self.__numVertice = n

    def __str__(self):
        return f"{self.nombre} ({self.numVertice})"
