'''
Created on 22 nov. 2020

@author: RSSpe
'''

from grafo_matriz import GrafoMatriz


def crearObjeto():
    while True:
        print("Ingresa tamaño de la matriz nxn")
        
        try:
            tamaño = int(input("Ingresa tamaño: "))
        
            if tamaño<=0:
                print("Tamaño no permitido")
            else:
                return GrafoMatriz(tamaño)
        except:
            print("Error en la entrada de datos, por favor vuelve a intentarlo")
        print()


gm = crearObjeto()

print()
print()
while True:
    print("--------------MENU------------------")
    print("1.- Introduce vertice")
    print("2.- Introduce arco")
    print("3.- Ver matriz")
    print("4.- Verificar si 2 vertices son adyacentes")
    print("5.- Salir")

    try:
        opcion = int(input("Introduce opcion: "))
        
        if opcion==1:
            if gm.numVerts!=len(gm.matAd[0]):
                vertice = input("Introduce vertice: ")
            
                gm.nuevoVertice(vertice)

            else:
                print("Limite de vertices alcanzado")

        elif opcion==2:
            
            try:
                print("Introduce primer vertice")
                p = input()
                print("Introduce segundo vertice")
                s = input()
            
                gm.nuevoArco(p, s)
            except Exception as e:
                print("Error, no existe algun vertice o ninguno")

        elif opcion==3:
            print()
            gm.imprimirFormato()
            print()
            
        elif opcion==4:
            print()
            vertice = input("Introduce primer vertice: ")
            vertice2 = input("Introduce segundo vertice: ")
            try:
                print(gm.adyacente(vertice, vertice2))
            except Exception:
                print("Error en los vertices")
        
        elif opcion==5:
            print()
            print("Programa terminado")
            break
            
        else:
            print("Opcion invalida, prueba de nuevo")

    except ValueError as e:
        print(f"Error <{e}>, prueba de nuevo")
    print()
