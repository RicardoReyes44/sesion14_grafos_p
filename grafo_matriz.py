'''
Created on 22 nov. 2020

@author: RSSpe
'''

from vertice import Vertice


class GrafoMatriz:

    maxVerts = 0

    def __init__(self, max):
        self.verts = []
        self.matAd = []
        self.numVerts = 0
        
        self.llenar(max)

    def llenar(self, max):
        for i in range(max):
            self.matAd.append([])
            for j in range(max):
                self.matAd[i].append(0)

    def imprimirFormato(self):
        print("------MATRIZ DE ADYACENCIA------")
        for i in range(len(self.matAd[0])):
            for j in range(len(self.matAd[0])):
                print(f"{self.matAd[i][j]} ", end="")
            print()
    
    def nuevoVertice(self, nom):
        esta = self.numVertice(nom) >= 0

        if not esta:
            v = Vertice(nom)
            v.asigVert(self.numVerts)
            self.verts.append(v)
            self.numVerts+=1

    def numVertice(self, vs):
        v = Vertice(vs)
        encontrado = False
        i = 0

        while not encontrado and i < self.numVerts:
            encontrado = self.verts[i].equals(v)
            if not encontrado:
                i+=1
        
        if i < self.numVerts:
            return i
        else:
            return -1

    def nuevoArco(self, a, b):
        va = self.numVertice(a)
        vb = self.numVertice(b)

        if (va < 0 or vb < 0):
            raise Exception ("El vértice no existe")
        self.matAd[va][vb] = 1
    
    def adyacente(self, a, b):
        va = self.numVertice(a);
        vb = self.numVertice(b);
        
        if (va < 0 or vb < 0):
            raise Exception ("El vértice no existe");
        return self.matAd[va][vb] == 1
